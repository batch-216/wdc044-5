package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);
    ResponseEntity updatePost(Long id, String stringToken, Post post);
    ResponseEntity deletePost(Long id, String stringToken);
    Iterable<Post> getPosts();
    Iterable<Post> userPosts(String stringToken);
}
